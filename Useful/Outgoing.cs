﻿using System;

namespace HabboEvents
{
    public static class Outgoing
    {
        public static int SendBannerMessageComposer;

        public static int SecretKeyComposer;

        public static int Ping;

        public static int AuthenticationOK;

        public static int HomeRoom;

        public static int FavouriteRooms;

        public static int FavsUpdate;

        public static int Fuserights;

        public static int bools1;

        public static int EndTalentPractise;

        public static int SendAllowances;

        public static int CitizenshipPanel;

        public static int LoadQuiz;

        public static int CheckQuiz;

        public static int ActivityPoints;

        public static int CreditsBalance;

        public static int UniqueID;

        public static int HabboInfomation;

        public static int ProfileInformation;

        public static int Allowances;

        public static int AchievementPoints;

        public static int SerializePublicRooms;

        public static int NavigatorPacket;

        public static int OpenShop;

        public static int OpenShopPage;

        public static int ShopData1;

        public static int AchievementData;

        public static int ShopData2;

        public static int Offer;

        public static int PetRace;

        public static int CheckPetName;

        public static int UpdateShop;

        public static int AchievementList;

        public static int AchievementProgress;

        public static int UnlockAchievement;

        public static int OpenModTools;

        public static int SendNotif;

        public static int ModResponse;

        public static int BroadcastMessage;

        public static int SerializeMontura;

        public static int GiftError;

        public static int UpdateInventary;

        public static int SerializePurchaseInformation;

        public static int SendPurchaseAlert;

        public static int PublicCategories;

        public static int EffectsInventary;

        public static int AddEffectToInventary;

        public static int EnableEffect;

        public static int StopEffect;

        public static int OpenHelpTool;

        public static int CanCreateRoom;

        public static int OnCreateRoomInfo;

        public static int RoomDataEdit;

        public static int UpdateRoomOne;

        public static int RoomAds;

        public static int PrepareRoomForUsers;

        public static int RoomErrorToEnter;

        public static int OutOfRoom;

        public static int RoomError;

        public static int DoorBellNoPerson;

        public static int Doorbell;

        public static int ValidDoorBell;

        public static int InitFriends;

        public static int InitRequests;

        public static int SendFriendRequest;

        public static int FriendUpdate;

        public static int InstantChat;

        public static int InstantInvite;

        public static int InstantChatError;

        public static int InitialRoomInformation;

        public static int FollowBuddy;

        public static int FollowBuddyError;

        public static int SearchFriend;

        public static int TradeStart;

        public static int TradeUpdate;

        public static int TradeAcceptUpdate;

        public static int TradeComplete;

        public static int TradeCloseClean;

        public static int TradeClose;

        public static int RoomDecoration;

        public static int RoomRightsLevel;

        public static int QuitRights;

        public static int HasOwnerRights;

        public static int RateRoom;

        public static int ScoreMeter;

        public static int CanCreateEvent;

        public static int RoomEvent;

        public static int HeightMap;

        public static int RelativeMap;

        public static int OpenGift;

        public static int SerializeFloorItems;

        public static int SerializeWallItems;

        public static int FloodFilter;

        public static int SetRoomUser;

        public static int ConfigureWallandFloor;

        public static int ValidRoom;

        public static int RoomData;

        public static int UpdateState;

        public static int SerializeClub;

        public static int ClubComposer;

        public static int PopularTags;

        public static int FlatCats;

        public static int Talk;

        public static int Shout;

        public static int Whisp;

        public static int UpdateIgnoreStatus;

        public static int GiveRespect;

        public static int PrepareCampaing;

        public static int SendCampaingData;

        public static int UpdateUserInformation;

        public static int Dance;

        public static int Action;

        public static int IdleStatus;

        public static int Inventory;

        public static int PetInventory;

        public static int PlaceBot;

        public static int PetInformation;

        public static int RespectPet;

        public static int AddExperience;

        public static int BadgesInventory;

        public static int UpdateBadges;

        public static int GetUserBadges;

        public static int GetUserTags;

        public static int GivePowers;

        public static int RemovePowers;

        public static int GetPowerList;

        public static int TypingStatus;

        public static int RemoveObjectFromInventory;

        public static int AddFloorItemToRoom;

        public static int AddWallItemToRoom;

        public static int UpdateItemOnRoom;

        public static int UpdateWallItemOnRoom;

        public static int UpdateFloorItemExtraData;

        public static int PickUpFloorItem;

        public static int PickUpWallItem;

        public static int WiredTrigger;

        public static int WiredEffect;

        public static int WiredCondition;

        public static int SaveWired;

        public static int UserLeftRoom;

        public static int RoomTool;

        public static int UserTool;

        public static int RoomChatlog;

        public static int UserChatlog;

        public static int RoomVisits;

        public static int IssueChatlog;

        public static int SerializeIssue;

        public static int UpdateIssue;

        public static int ApplyEffects;

        public static int ApplyCarryItem;

        public static int ObjectOnRoller;

        public static int DimmerData;

        public static int OpenPostIt;

        public static int UpdateFreezeLives;

        public static int WardrobeData;

        public static int HelpRequest;

        public static int SerializeCompetitionWinners;

        public static int InitStream;

        public static int SetCommandsView;

        public static int PetTrainPanel;

        public static int LoadQuests;

        public static int ActivateQuest;

        public static int QuitAlertQ;

        public static int CompleteQuests;

        public static int GetName;

        public static int CheckName;

        public static int ChangePublicName;

        public static int UpdateRoom;

        public static int CanSell;

        public static int AddMarket;

        public static int MySells;

        public static int QuitMarket;

        public static int GetOffers;

        public static int BuyItemMarket;

        public static int AddMarket2;

        public static int ListenPreviewSong;

        public static int SerializeInvSongs;

        public static int SerializeJukeSongs;

        public static int PlayStopMusic;

        public static int SendGuildParts;

        public static int SendGuildElements;

        public static int SendHtmlColors;

        public static int SendRoomAndGroup;

        public static int SendAdvGroupInit;

        public static int SendGestionGroup;

        public static int SendMembersAndPetitions;

        public static int AddNewMember;

        public static int UpdatePetitionsGuild;

        public static int SendGroup;

        public static int RemoveGuildFavorite;

        public static int GetRankInGame;

        public static int CreateWar;

        public static int AddToNewGame;

        public static int LeaveGame;

        public static int StartCounter;

        public static int SetStep1;

        public static int Game2EnterArenaMessageEvent;

        public static int Game2ArenaEnteredMessageEvent;

        public static int Game2StageStillLoadingMessageEvent;

        public static int Game2StageLoadMessageEvent;

        public static int Game2StageStartingMessageEvent;

        public static int Game2GameChatFromPlayerMessageEvent;

        public static int Game2StageRunningMessageEvent;

        public static int Game2PlayerExitedGameArenaMessageEvent;

        public static int Game2GameStatusMessageEvent;

        static Outgoing()
        {
            Outgoing.SendBannerMessageComposer = 3500;
            Outgoing.SecretKeyComposer = 659;
            Outgoing.Ping = 1080;
            Outgoing.AuthenticationOK = 1065;
            Outgoing.HomeRoom = 1994;
            Outgoing.FavouriteRooms = 2429;
            Outgoing.FavsUpdate = 3068;
            Outgoing.Fuserights = 978;
            Outgoing.bools1 = 1060;
            Outgoing.EndTalentPractise = 840;
            Outgoing.SendAllowances = 1064;
            Outgoing.CitizenshipPanel = 3244;
            Outgoing.LoadQuiz = 1652;
            Outgoing.CheckQuiz = 68;
            Outgoing.ActivityPoints = 2542;
            Outgoing.CreditsBalance = 2995;
            Outgoing.UniqueID = 1130;
            Outgoing.HabboInfomation = 2228;
            Outgoing.ProfileInformation = 2776;
            Outgoing.Allowances = 519;
            Outgoing.AchievementPoints = 1614;
            Outgoing.SerializePublicRooms = 3700;
            Outgoing.NavigatorPacket = 2160;
            Outgoing.OpenShop = 2563;
            Outgoing.OpenShopPage = 2826;
            Outgoing.ShopData1 = 1242;
            Outgoing.ShopData2 = 2889;
            Outgoing.Offer = 2437;
            Outgoing.PetRace = 1721;
            Outgoing.CheckPetName = 3771;
            Outgoing.UpdateShop = 1956;
            Outgoing.AchievementList = 1576;
            Outgoing.AchievementProgress = 1888;
            Outgoing.UnlockAchievement = 1173;
            Outgoing.OpenModTools = 2659;
            Outgoing.SendNotif = 3192;
            Outgoing.ModResponse = 904;
            Outgoing.BroadcastMessage = 2491;
            Outgoing.SerializeMontura = 3397;
            Outgoing.GiftError = 2612;
            Outgoing.UpdateInventary = 3586;
            Outgoing.SerializePurchaseInformation = 1128;
            Outgoing.SendPurchaseAlert = 1642;
            Outgoing.PublicCategories = 3700;
            Outgoing.EffectsInventary = 2938;
            Outgoing.AddEffectToInventary = 1722;
            Outgoing.EnableEffect = 1569;
            Outgoing.StopEffect = 3650;
            Outgoing.OpenHelpTool = 1500;
            Outgoing.CanCreateRoom = 3859;
            Outgoing.OnCreateRoomInfo = 290;
            Outgoing.RoomDataEdit = 3975;
            Outgoing.UpdateRoomOne = 1926;
            Outgoing.RoomAds = 1475;
            Outgoing.PrepareRoomForUsers = 3798;
            Outgoing.RoomErrorToEnter = 2945;
            Outgoing.OutOfRoom = 567;
            Outgoing.RoomError = 2120;
            Outgoing.DoorBellNoPerson = 3941;
            Outgoing.Doorbell = 265;
            Outgoing.ValidDoorBell = 3374;
            Outgoing.InitFriends = 398;
            Outgoing.InitRequests = 1671;
            Outgoing.SendFriendRequest = 3858;
            Outgoing.FriendUpdate = 1141;
            Outgoing.InstantChat = 399;
            Outgoing.InstantInvite = 2073;
            Outgoing.InstantChatError = 412;
            Outgoing.InitialRoomInformation = 999;
            Outgoing.FollowBuddy = 944;
            Outgoing.FollowBuddyError = 1109;
            Outgoing.SearchFriend = 812;
            Outgoing.TradeStart = 2567;
            Outgoing.TradeUpdate = 1685;
            Outgoing.TradeAcceptUpdate = 1168;
            Outgoing.TradeComplete = 59;
            Outgoing.TradeCloseClean = 303;
            Outgoing.TradeClose = 433;
            Outgoing.RoomDecoration = 3663;
            Outgoing.RoomRightsLevel = 1496;
            Outgoing.QuitRights = 1519;
            Outgoing.HasOwnerRights = 213;
            Outgoing.RateRoom = 3401;
            Outgoing.ScoreMeter = 3401;
            Outgoing.CanCreateEvent = 2099;
            Outgoing.RoomEvent = 743;
            Outgoing.HeightMap = 9;
            Outgoing.RelativeMap = 2483;
            Outgoing.OpenGift = 1208;
            Outgoing.SerializeFloorItems = 3580;
            Outgoing.SerializeWallItems = 3096;
            Outgoing.FloodFilter = 1641;
            Outgoing.SetRoomUser = 2204;
            Outgoing.ConfigureWallandFloor = 939;
            Outgoing.ValidRoom = 1084;
            Outgoing.RoomData = 3116;
            Outgoing.UpdateState = 493;
            Outgoing.SerializeClub = 2974;
            Outgoing.ClubComposer = 3935;
            Outgoing.PopularTags = 1574;
            Outgoing.FlatCats = 1154;
            Outgoing.Talk = 2119;
            Outgoing.Shout = 3165;
            Outgoing.Whisp = 701;
            Outgoing.UpdateIgnoreStatus = 2600;
            Outgoing.GiveRespect = 2283;
            Outgoing.PrepareCampaing = 3745;
            Outgoing.SendCampaingData = 1064;
            Outgoing.UpdateUserInformation = 421;
            Outgoing.Dance = 3301;
            Outgoing.Action = 523;
            Outgoing.IdleStatus = 746;
            Outgoing.Inventory = 177;
            Outgoing.PetInventory = 3678;
            Outgoing.PlaceBot = 2204;
            Outgoing.PetInformation = 3529;
            Outgoing.RespectPet = 1773;
            Outgoing.AddExperience = 994;
            Outgoing.BadgesInventory = 3148;
            Outgoing.UpdateBadges = 178;
            Outgoing.GetUserBadges = 178;
            Outgoing.GetUserTags = 2884;
            Outgoing.GivePowers = 3945;
            Outgoing.RemovePowers = 3889;
            Outgoing.GetPowerList = 703;
            Outgoing.TypingStatus = 851;
            Outgoing.RemoveObjectFromInventory = 606;
            Outgoing.AddFloorItemToRoom = 1243;
            Outgoing.AddWallItemToRoom = 165;
            Outgoing.UpdateItemOnRoom = 2565;
            Outgoing.UpdateWallItemOnRoom = 783;
            Outgoing.UpdateFloorItemExtraData = 3124;
            Outgoing.PickUpFloorItem = 2113;
            Outgoing.PickUpWallItem = 2208;
            Outgoing.WiredTrigger = 3393;
            Outgoing.WiredEffect = 2189;
            Outgoing.WiredCondition = 2423;
            Outgoing.SaveWired = 2466;
            Outgoing.UserLeftRoom = 3770;
            Outgoing.RoomTool = 1180;
            Outgoing.UserTool = 1231;
            Outgoing.RoomChatlog = 3800;
            Outgoing.UserChatlog = 2287;
            Outgoing.RoomVisits = 1470;
            Outgoing.IssueChatlog = 1999;
            Outgoing.SerializeIssue = 1775;
            Outgoing.UpdateIssue = 226;
            Outgoing.ApplyEffects = 329;
            Outgoing.ApplyCarryItem = 1773;
            Outgoing.ObjectOnRoller = 1508;
            Outgoing.DimmerData = 2078;
            Outgoing.OpenPostIt = 2768;
            Outgoing.UpdateFreezeLives = 2845;
            Outgoing.WardrobeData = 3443;
            Outgoing.HelpRequest = 1314;
            Outgoing.SerializeCompetitionWinners = 827;
            Outgoing.InitStream = 2143;
            Outgoing.SetCommandsView = 2307;
            Outgoing.PetTrainPanel = 971;
            Outgoing.LoadQuests = 3361;
            Outgoing.ActivateQuest = 1881;
            Outgoing.QuitAlertQ = 435;
            Outgoing.CompleteQuests = 1638;
            Outgoing.GetName = 588;
            Outgoing.CheckName = 3839;
            Outgoing.ChangePublicName = 355;
            Outgoing.UpdateRoom = 3530;
            Outgoing.AchievementData = 1225;
            Outgoing.CanSell = 645;
            Outgoing.AddMarket = 1468;
            Outgoing.MySells = 1900;
            Outgoing.QuitMarket = 1178;
            Outgoing.GetOffers = 1294;
            Outgoing.BuyItemMarket = 2182;
            Outgoing.AddMarket2 = 3586;
            Outgoing.ListenPreviewSong = 976;
            Outgoing.SerializeInvSongs = 3173;
            Outgoing.SerializeJukeSongs = 3318;
            Outgoing.PlayStopMusic = 1528;
            Outgoing.SendGuildParts = 862;
            Outgoing.SendGuildElements = 873;
            Outgoing.SendHtmlColors = 3640;
            Outgoing.SendRoomAndGroup = 1093;
            Outgoing.SendAdvGroupInit = 3463;
            Outgoing.SendGestionGroup = 3352;
            Outgoing.SendMembersAndPetitions = 1491;
            Outgoing.AddNewMember = 255;
            Outgoing.UpdatePetitionsGuild = 3649;
            Outgoing.SendGroup = 1696;
            Outgoing.RemoveGuildFavorite = 1928;
            Outgoing.GetRankInGame = 1414;
            Outgoing.CreateWar = 2737;
            Outgoing.AddToNewGame = 2200;
            Outgoing.LeaveGame = 3543;
            Outgoing.StartCounter = 2155;
            Outgoing.SetStep1 = 3828;
            Outgoing.Game2EnterArenaMessageEvent = 811;
            Outgoing.Game2ArenaEnteredMessageEvent = 2312;
            Outgoing.Game2StageStillLoadingMessageEvent = 1048;
            Outgoing.Game2StageLoadMessageEvent = 1125;
            Outgoing.Game2StageStartingMessageEvent = 2942;
            Outgoing.Game2GameChatFromPlayerMessageEvent = 483;
            Outgoing.Game2StageRunningMessageEvent = 2975;
            Outgoing.Game2PlayerExitedGameArenaMessageEvent = 2050;
            Outgoing.Game2GameStatusMessageEvent = 275;
        }
    }
}