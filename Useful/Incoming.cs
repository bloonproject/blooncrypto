﻿using System;

namespace HabboEvents
{
    public static class Incoming
    {
        public static int CheckReleaseMessageEvent;

        public static int InitCrypto;

        public static int SecretKey;

        public static int ClientVars;

        public static int UniqueMachineID;

        public static int SSOTicket;

        public static int UserInformation;

        public static int SerializeClub;

        public static int LoadCategorys;

        public static int Pong;

        public static int Ping;

        public static int EndQuiz;

        public static int LoadUser2;

        public static int OpenHelpTool;

        public static int GetWardrobe;

        public static int SaveWardrobe;

        public static int OpenAchievements;

        public static int CatalogData1;

        public static int CatalogData2;

        public static int ExtraVipWindow;

        public static int LoadTalents;

        public static int OpenCatalog;

        public static int OpenCatalogPage;

        public static int CatalogGetRace;

        public static int CheckPetName;

        public static int PurchaseCatalogItem;

        public static int PurchaseGift;

        public static int OpenGift;

        public static int StartEffect;

        public static int EnableEffect;

        public static int RedeemVoucher;

        public static int LoadFeaturedRooms;

        public static int LoadAllRooms;

        public static int LoadMyRooms;

        public static int LoadPopularTags;

        public static int RoomsWhereMyFriends;

        public static int RoomsOfMyFriends;

        public static int MyFavs;

        public static int RecentRooms;

        public static int HighRatedRooms;

        public static int SearchRoomByName;

        public static int CanCreateRoom;

        public static int CreateRoom;

        public static int GetRoomData;

        public static int SaveRoomData;

        public static int LoadFirstRoomData;

        public static int LoadHeightMap;

        public static int UpdateAllMount;

        public static int AddUserToRoom;

        public static int AddUserToRoom2;

        public static int LookTo;

        public static int StartTrade;

        public static int SendOffer;

        public static int CancelOffer;

        public static int AcceptTrade;

        public static int UnacceptTrade;

        public static int ConfirmTrade;

        public static int CancelTrade;

        public static int Move;

        public static int Talk;

        public static int Shout;

        public static int SendRespects;

        public static int ChangeLook;

        public static int ChangeMotto;

        public static int ApplyDance;

        public static int ApplySign;

        public static int ApplyAction;

        public static int Sit;

        public static int GetFriends;

        public static int FriendRequest;

        public static int AcceptRequest;

        public static int DeclineRequest;

        public static int SendInstantMessenger;

        public static int FollowFriend;

        public static int UpdateFriendsState;

        public static int InviteFriendsToMyRoom;

        public static int SearchFriend;

        public static int DeleteFriend;

        public static int LoadProfile;

        public static int OpenInventory;

        public static int PetInventary;

        public static int PlacePet;

        public static int AnswerQuestion;

        public static int PickupPet;

        public static int RespetPet;

        public static int AddSaddleToPet;

        public static int RemoveSaddle;

        public static int MountOnPet;

        public static int PetInfo;

        public static int ApplySpace;

        public static int AddFloorItem;

        public static int Mannequinshit;

        public static int MoveOrRotate;

        public static int MoveWall;

        public static int HandleItem;

        public static int HandleWallItem;

        public static int StartMoodlight;

        public static int TurnOnMoodlight;

        public static int ApplyMoodlightChanges;

        public static int AddPostIt;

        public static int OpenPostIt;

        public static int SavePostIt;

        public static int DeletePostIt;

        public static int SetHome;

        public static int RemoveRoom;

        public static int RedeemExchangeFurni;

        public static int StartTyping;

        public static int StopTyping;

        public static int GiveRights;

        public static int RemoveAllRights;

        public static int RemoveRightsFrom;

        public static int PickupItem;

        public static int SaveWiredTrigger;

        public static int SaveWiredEffect;

        public static int ToolForThisRoom;

        public static int ToolForUser;

        public static int GetRoomVisits;

        public static int UserChatlog;

        public static int SendMessageByTemplate;

        public static int ModActionKickUser;

        public static int ModActionMuteUser;

        public static int ModActionBanUser;

        public static int SendUserMessage;

        public static int CreateTicket;

        public static int PickIssue;

        public static int IssueChatlog;

        public static int CloseIssue;

        public static int ReleaseIssue;

        public static int PerformRoomAction;

        public static int OpenRoomChatlog;

        public static int GiveRoomScore;

        public static int SendRoomAlert;

        public static int KickUserOfRoom;

        public static int BanUserOfRoom;

        public static int IgnoreUser;

        public static int UnignoreUser;

        public static int AnswerDoorBell;

        public static int ReqLoadByDoorBell;

        public static int AddFavourite;

        public static int RemoveFavourite;

        public static int Whisp;

        public static int BadgesInventary;

        public static int BotsInventary;

        public static int ApplyBadge;

        public static int GetUserBadges;

        public static int GetUserTags;

        public static int GoToHotelView;

        public static int RemoveHanditem;

        public static int GiveObject;

        public static int OpenDice;

        public static int RunDice;

        public static int TrainPet;

        public static int SaveConditional;

        public static int OpenQuests;

        public static int ActiveQuests;

        public static int CancelQuests;

        public static int ActiveEndedQuest;

        public static int CheckNameChange;

        public static int SaveNameChange;

        public static int ListenPreview;

        public static int LoadInvSongs;

        public static int LoadJukeSongs;

        public static int AddNewCdToJuke;

        public static int RemoveCdToJuke;

        public static int MarketplaceCanSell;

        public static int MarketplacePostItem;

        public static int MarketplaceGetOwnOffers;

        public static int MarketplaceTakeBack;

        public static int MarketplaceClaimCredits;

        public static int MarketplaceGetOffers;

        public static int MarketplacePurchase;

        public static int OpenGuildPage;

        public static int BuyGroup;

        public static int EndConfirmBuy;

        public static int EditGuild;

        public static int EditIdentidad;

        public static int EditPlaca;

        public static int EditColores;

        public static int EditAjustes;

        public static int PopularGuilds;

        public static int ExitGuild;

        public static int SendRequestGuild;

        public static int LoadMembersPetitions;

        public static int AcceptMember;

        public static int UpdateUserToRankGuild;

        public static int SendFurniGuild;

        public static int GetGuildFavorite;

        public static int RemoveGuildFavorite;

        public static int SaveBranding;

        public static int StartQuiz;

        public static int GetGames;

        public static int GetGame;

        public static int StartPanel;

        public static int Game2LeaveGameMessageComposer;

        public static int Game2GameChatMessageComposer;

        public static int Game2SetUserMoveTargetMessageComposer;

        public static int InitStream;

        public static int StreamLike;

        public static int CreateStream;

        public static int SearchStream;

        static Incoming()
        {
            Incoming.CheckReleaseMessageEvent = 4000;
            Incoming.InitCrypto = 2996;
            Incoming.SecretKey = 840;
            Incoming.ClientVars = 3561;
            Incoming.UniqueMachineID = 3570;
            Incoming.SSOTicket = 442;
            Incoming.UserInformation = 3183;
            Incoming.SerializeClub = 2757;
            Incoming.LoadCategorys = 2570;
            Incoming.Pong = 121;
            Incoming.Ping = 67;
            Incoming.EndQuiz = 1741;
            Incoming.LoadUser2 = 858;
            Incoming.OpenHelpTool = 456;
            Incoming.GetWardrobe = 3885;
            Incoming.SaveWardrobe = 383;
            Incoming.OpenAchievements = 301;
            Incoming.CatalogData1 = 3549;
            Incoming.CatalogData2 = 1973;
            Incoming.ExtraVipWindow = 1936;
            Incoming.LoadTalents = 64;
            Incoming.OpenCatalog = 3038;
            Incoming.OpenCatalogPage = 1132;
            Incoming.CatalogGetRace = 1875;
            Incoming.CheckPetName = 467;
            Incoming.PurchaseCatalogItem = 3487;
            Incoming.PurchaseGift = 553;
            Incoming.OpenGift = 225;
            Incoming.SaveBranding = 242;
            Incoming.StartEffect = 1006;
            Incoming.EnableEffect = 1423;
            Incoming.RedeemVoucher = 2265;
            Incoming.LoadFeaturedRooms = 1930;
            Incoming.LoadAllRooms = 2958;
            Incoming.LoadMyRooms = 3350;
            Incoming.LoadPopularTags = 2086;
            Incoming.RoomsWhereMyFriends = 3649;
            Incoming.RoomsOfMyFriends = 2623;
            Incoming.MyFavs = 3414;
            Incoming.RecentRooms = 557;
            Incoming.HighRatedRooms = 2390;
            Incoming.SearchRoomByName = 3891;
            Incoming.CanCreateRoom = 2115;
            Incoming.CreateRoom = 2431;
            Incoming.GetRoomData = 1677;
            Incoming.SaveRoomData = 1815;
            Incoming.LoadFirstRoomData = 1650;
            Incoming.LoadHeightMap = 562;
            Incoming.UpdateAllMount = 1148;
            Incoming.AddUserToRoom = 2346;
            Incoming.AddUserToRoom2 = 1188;
            Incoming.LookTo = 3714;
            Incoming.StartTrade = 3314;
            Incoming.SendOffer = 1909;
            Incoming.CancelOffer = 2654;
            Incoming.AcceptTrade = 3847;
            Incoming.UnacceptTrade = 1426;
            Incoming.ConfirmTrade = 3402;
            Incoming.CancelTrade = 311;
            Incoming.Move = 703;
            Incoming.Talk = 1548;
            Incoming.Shout = 1953;
            Incoming.SendRespects = 1198;
            Incoming.ChangeLook = 1332;
            Incoming.ChangeMotto = 3603;
            Incoming.ApplyDance = 1157;
            Incoming.ApplySign = 1879;
            Incoming.ApplyAction = 499;
            Incoming.Sit = 3679;
            Incoming.GetFriends = 1237;
            Incoming.FriendRequest = 2913;
            Incoming.AcceptRequest = 2665;
            Incoming.DeclineRequest = 2481;
            Incoming.SendInstantMessenger = 90;
            Incoming.FollowFriend = 2289;
            Incoming.UpdateFriendsState = 2621;
            Incoming.InviteFriendsToMyRoom = 1490;
            Incoming.SearchFriend = 1487;
            Incoming.DeleteFriend = 2594;
            Incoming.LoadProfile = 1611;
            Incoming.OpenInventory = 964;
            Incoming.PetInventary = 1831;
            Incoming.PlacePet = 2461;
            Incoming.AnswerQuestion = 3581;
            Incoming.PickupPet = 2759;
            Incoming.RespetPet = 3642;
            Incoming.AddSaddleToPet = 3937;
            Incoming.RemoveSaddle = 1984;
            Incoming.MountOnPet = 1233;
            Incoming.PetInfo = 891;
            Incoming.ApplySpace = 623;
            Incoming.AddFloorItem = 3078;
            Incoming.Mannequinshit = 2242;
            Incoming.MoveOrRotate = 2792;
            Incoming.MoveWall = 2863;
            Incoming.HandleItem = 2417;
            Incoming.HandleWallItem = 2622;
            Incoming.StartMoodlight = 1823;
            Incoming.TurnOnMoodlight = 3249;
            Incoming.ApplyMoodlightChanges = 3338;
            Incoming.AddPostIt = 3918;
            Incoming.OpenPostIt = 1653;
            Incoming.SavePostIt = 2768;
            Incoming.DeletePostIt = 3426;
            Incoming.SetHome = 3896;
            Incoming.RemoveRoom = 3856;
            Incoming.RedeemExchangeFurni = 195;
            Incoming.StartTyping = 1123;
            Incoming.StopTyping = 3916;
            Incoming.GiveRights = 1283;
            Incoming.RemoveAllRights = 1396;
            Incoming.RemoveRightsFrom = 3638;
            Incoming.PickupItem = 2805;
            Incoming.SaveWiredTrigger = 776;
            Incoming.SaveWiredEffect = 2134;
            Incoming.ToolForThisRoom = 1847;
            Incoming.ToolForUser = 3530;
            Incoming.GetRoomVisits = 2761;
            Incoming.UserChatlog = 2101;
            Incoming.SendMessageByTemplate = 3053;
            Incoming.ModActionKickUser = 391;
            Incoming.ModActionMuteUser = 798;
            Incoming.ModActionBanUser = 1038;
            Incoming.SendUserMessage = 987;
            Incoming.CreateTicket = 22;
            Incoming.PickIssue = 791;
            Incoming.IssueChatlog = 3605;
            Incoming.CloseIssue = 843;
            Incoming.ReleaseIssue = 3471;
            Incoming.PerformRoomAction = 2560;
            Incoming.OpenRoomChatlog = 2620;
            Incoming.GiveRoomScore = 1243;
            Incoming.SendRoomAlert = 2998;
            Incoming.KickUserOfRoom = 12;
            Incoming.BanUserOfRoom = 3746; // oder 1515.
            Incoming.IgnoreUser = 1141;
            Incoming.UnignoreUser = 2377;
            Incoming.AnswerDoorBell = 2116;
            Incoming.ReqLoadByDoorBell = 356;
            Incoming.AddFavourite = 2634;
            Incoming.RemoveFavourite = 249;
            Incoming.Whisp = 2512;
            Incoming.BadgesInventary = 3313;
            Incoming.BotsInventary = 2680;
            Incoming.ApplyBadge = 1451;
            Incoming.GetUserBadges = 2756;
            Incoming.GetUserTags = 3030;
            Incoming.GoToHotelView = 2718;
            Incoming.RemoveHanditem = 1124;
            Incoming.GiveObject = 2755;
            Incoming.OpenDice = 1374;
            Incoming.RunDice = 409;
            Incoming.TrainPet = 2127;
            Incoming.SaveConditional = 2287;
            Incoming.OpenQuests = 657;
            Incoming.ActiveQuests = 2689;
            Incoming.CancelQuests = 3462;
            Incoming.ActiveEndedQuest = 3145;
            Incoming.CheckNameChange = 256;
            Incoming.SaveNameChange = 3930;
            Incoming.ListenPreview = 2112;
            Incoming.LoadInvSongs = 3718;
            Incoming.LoadJukeSongs = 3862;
            Incoming.AddNewCdToJuke = 1153;
            Incoming.RemoveCdToJuke = 1178;
            Incoming.MarketplaceCanSell = 1942;
            Incoming.MarketplacePostItem = 2491;
            Incoming.MarketplaceGetOwnOffers = 2726;
            Incoming.MarketplaceTakeBack = 3620;
            Incoming.MarketplaceClaimCredits = 1088;
            Incoming.MarketplaceGetOffers = 1289;
            Incoming.MarketplacePurchase = 3693; // Ima war hier.
            Incoming.OpenGuildPage = 2005;
            Incoming.BuyGroup = 1258;
            Incoming.EndConfirmBuy = 1787;
            Incoming.EditGuild = 2205;
            Incoming.EditIdentidad = 3122;
            Incoming.EditPlaca = 609;
            Incoming.EditColores = 2427;
            Incoming.EditAjustes = 2537;
            Incoming.PopularGuilds = 3152;
            Incoming.ExitGuild = 597;
            Incoming.SendRequestGuild = 3671;
            Incoming.LoadMembersPetitions = 1923;
            Incoming.AcceptMember = 2959;
            Incoming.UpdateUserToRankGuild = 3069;
            Incoming.SendFurniGuild = 607;
            Incoming.GetGuildFavorite = 3523;
            Incoming.RemoveGuildFavorite = 2394;
            Incoming.StartQuiz = 2510;
            Incoming.GetGames = 2498;
            Incoming.GetGame = 39;
            Incoming.StartPanel = 1508;
            Incoming.Game2LeaveGameMessageComposer = 42;
            Incoming.Game2GameChatMessageComposer = 1462;
            Incoming.Game2SetUserMoveTargetMessageComposer = 1630;
            Incoming.InitStream = 2626;
            Incoming.StreamLike = 1816;
            Incoming.CreateStream = 1919;
            Incoming.SearchStream = 1430;
        }
    }
}