<?php
/*
 * BloonCrypto
 * Habbo R63 Post-Shuffle
 * Based on the work of Burak, edited by BloonCrypto Git Community. (skype: burak.karamahmut)
 * 
 * https://github.com/BurakDev/BloonProject/tree/BloonCrypto
 */
Class User{
	var $id;
	var $socket;
	var $ip;
	var $port;
	var $countconnection;
	var $userid;
	var $username;
	var $mail;
	var $rank;
	var $credits;
	var $vip_points;
	var $activity_points;
	var $look;
	var $gender;
	var $motto;
	var $account_created;
	var $last_online;
	var $home_room;
	var $respect;
	var $daily_respect_points;
	var $daily_pet_respect_points;
	var $block_newfriends;
	var $hide_online;
	var $hide_inroom;
	var $vip;
	var $volume;
	var $accept_trading;
	var $room_id;
	var $pos_x;
	var $pos_y;
	var $pos_z;
}
?>